FROM mdillon/postgis:9.6
MAINTAINER Ravi Shekhar <ravi+docker@shekhar.info>

RUN apt-get update -y -qq && \
    apt-get -y -qq install protobuf-c-compiler \
     libprotobuf-c0-dev unzip git build-essential \
     ca-certificates postgresql-server-dev-9.6
    

WORKDIR /usr/src
RUN git clone https://github.com/citusdata/cstore_fdw.git && \
    cd cstore_fdw && \
    make -j"$(nproc)" && \
    make install && \
    cd .. && \
    rm -rf cstore_fdw && \
    apt-get remove -y build-essential git  && \
    apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/*

COPY ./init-cstore-fdw.sh /docker-entrypoint-initdb.d/cstore_fdw.sh
