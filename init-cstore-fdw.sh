#!/bin/sh

set -e

echo "shared_preload_libraries = 'cstore_fdw'" >> ${PGDATA}/postgresql.conf

service postgresql restart


# Perform all actions as $POSTGRES_USER
export PGUSER="$POSTGRES_USER"

"${psql[@]}" --dbname="$DB" <<-'EOSQL'
     CREATE EXTENSION cstore_fdw;
     CREATE SERVER cstore_server FOREIGN DATA WRAPPER cstore_fdw;
EOSQL
